<?php
/**
 * Created by PhpStorm.
 * User: kpawla13
 * Date: 2018-05-28
 * Time: 09:34
 */

require_once('./DataMatrix.php');

class LocalSearch
{

    public $optimumSolution = 0;
    public $optimumPermutation = [];
    public $chainSize = 0;
    public $dataMatrix = null;
    public $originalList = [];
    public $currentNeighbours = [];
    public $oldOptimum = null;
    public $oldOptimumPermutation = null;
    public $calculatedOptimumSolution = null;
    public $calculatedOptimumPermutation = null;
    public $permutationsAnalysed = 0;
    public $steps = 0;
    public $executionTime = 0;

    public function __construct($solutionFile, $dataFile)
    {

        $this->dataMatrix = new DataMatrix($dataFile);

        $this->optimalSolutionFromFile($solutionFile);
    }

    public function optimalSolutionFromFile($file)
    {

        try {

            $handle = fopen($file, "r");

            if ($handle) {

                $line = fgets($handle);
                $line = preg_replace('/\s+/', ' ', $line);
                $vals = explode(' ', trim($line, ' '));
                $this->chainSize = intval($vals[0]);
                $this->optimumSolution = intval($vals[1]);

                while (($line = fgets($handle)) != false) {

                    $line = preg_replace('/\s+/', ' ', $line);

                    if (strlen(trim($line, ' ')) == 0) continue;

                    $row = explode(' ', trim($line, ' '));
                    $row = array_map('intval', $row);

                    array_walk($row, function (&$item) {
                        $item--;
                    });

                    $this->optimumPermutation = array_merge($this->optimumPermutation, $row);

                }

                fclose($handle);

            } else {
                throw new Exception('Blad podczas otwierania pliku!');
            }
        } catch (Exception $e) {

            echo $e->getMessage();

            $this->chainSize = 0;
            $this->optimumSolution = 0;
            $this->optimumPermutation = [];

            exit();

        }

    }

    private function permOfIndex($orig, $start)
    {
        $result = [];

        for ($i = $start - 1; $i >= 0; $i--) {

            $newArr = $orig;

            $_tmp = $newArr[$start];
            $newArr[$start] = $newArr[$i];
            $newArr[$i] = $_tmp;

            $result[] = $newArr;

        }

        return $result;
    }

    public function generateNeighbours($arr)
    {

        $this->currentNeighbours = [];

        $st = count($arr) - 1;

        for ($c = $st; $c > 0; $c--) {

            $permutation = $this->permOfIndex($arr, $c);

            $this->currentNeighbours = array_merge($this->currentNeighbours, $permutation);

        }

    }

    public function beGreedy()
    {

        $this->steps++;

        foreach ($this->currentNeighbours as $neighbour) {

            $this->permutationsAnalysed++;

            $cost = 0;

            foreach ($neighbour as $disX => $flowX) {

                foreach ($neighbour as $disY => $flowY) {

                    $distanceMultiplier = $this->dataMatrix->getDistance($disX, $disY);
                    $flowMultiplier = $this->dataMatrix->getFlow($flowX, $flowY);

                    $cost += ($distanceMultiplier * $flowMultiplier);

                }

            }

            if ($this->calculatedOptimumSolution == null) {

                $this->calculatedOptimumSolution = $cost;
                $this->calculatedOptimumPermutation = $neighbour;

            } else {

                if ($cost < $this->calculatedOptimumSolution) {

                    $this->calculatedOptimumSolution = $cost;
                    $this->calculatedOptimumPermutation = $neighbour;
                    $this->generateNeighbours($neighbour);
                    $this->beGreedy();
                }

            }

        }

    }

    public function beSteepest()
    {

        $this->steps++;

        foreach ($this->currentNeighbours as $neighbour) {

            $this->permutationsAnalysed++;

            $cost = 0;

            foreach ($neighbour as $disX => $flowX) {

                foreach ($neighbour as $disY => $flowY) {

                    $distanceMultiplier = $this->dataMatrix->getDistance($disX, $disY);
                    $flowMultiplier = $this->dataMatrix->getFlow($flowX, $flowY);

                    $cost += ($distanceMultiplier * $flowMultiplier);

                }

            }

            if ($this->calculatedOptimumSolution == null) {

                $this->calculatedOptimumSolution = $cost;
                $this->oldOptimum = $cost;

                $this->calculatedOptimumPermutation = $neighbour;
                $this->oldOptimumPermutation = $neighbour;

            } else {

                if ($cost < $this->calculatedOptimumSolution) {

                    $this->calculatedOptimumSolution = $cost;
                    $this->calculatedOptimumPermutation = $neighbour;
                }

            }

        }

        if ($this->oldOptimum > $this->calculatedOptimumSolution) { //znalezlismy lepsze rozwiazanie wsrod sasiadow

            $this->oldOptimum = $this->calculatedOptimumSolution;
            $this->oldOptimumPermutation = $this->calculatedOptimumPermutation;

            $this->generateNeighbours($this->calculatedOptimumPermutation);
            $this->beSteepest();
        } else {
            $this->calculatedOptimumSolution = $this->oldOptimum;
            $this->calculatedOptimumPermutation = $this->oldOptimumPermutation;
        }

    }

    public function beRandom()
    {

        $cost = 0;

        $permutationChain = $this->originalList;

        shuffle($permutationChain);

        foreach ($permutationChain as $disX => $flowX) {

            foreach ($permutationChain as $disY => $flowY) {

                $distanceMultiplier = $this->dataMatrix->getDistance($disX, $disY);
                $flowMultiplier = $this->dataMatrix->getFlow($flowX, $flowY);

                $cost += ($distanceMultiplier * $flowMultiplier);

            }

        }

        if ($this->calculatedOptimumSolution == null) {

            $this->calculatedOptimumSolution = $cost;
            $this->calculatedOptimumPermutation = $permutationChain;
        } else {

            if ($cost < $this->calculatedOptimumSolution) {

                $this->calculatedOptimumSolution = $cost;
                $this->calculatedOptimumPermutation = $permutationChain;

            }

        }

    }
}