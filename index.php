<?php
/**
 * Created by PhpStorm.
 * User: kpawla13
 * Date: 2018-05-28
 * Time: 09:34
 */

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>QAP Karol Pawlak</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <script src="jquery-3.1.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="global.js"></script>
    </head>
    <body style="padding: 20px;">
        <div class="row">
            <div class="col-md-12" style="text-align: center;">

                <h3>Karol Pawlak (134498)</h3>
                <h4>Metody Optymalizacji</h4>
                <h5>Poszukiwanie rozwiązań dla problemu kwadratowego przypisania</h5>

            </div>
        </div>
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-4">


            </div>
            <div class="col-md-4">
                <a class="btn btn-block btn-success count-qap">Licz mnie te QAP'y</a>
            </div>

            <div class="col-md-4">
            </div>
        </div>
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="description" style="width: 100%; text-align: center;">Program wykona testy na 10 instancjach w trybie multi random start (10 przebiegów dla każdej instancji, losowy punkt startowy). Każda instancja problemu zostanie rozwiązana za pomocą 3 algorytmów - Greedy, Steepest oraz Random. Po ukończeniu, wyniki będa dostępne w pliku CSV.</div>
                <div class="progress" style="display: none;">
                    <div class="progress-bar" role="progressbar" style="width: 1%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">1%</div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>

    </body>

</html>