$(document).ready(function(){

    $(document).on('click', '.count-qap', function(){

        $('.count-qap').text('Czekaj na plik CSV...');

        goCountQAP();

    })

});

function goCountQAP() {
    $('.progress-bar').css('width', '1%').html('1%');
    $('.description').hide();
    $('.progress').show();

    $.ajax({
        url: "local.php",
        method: 'GET',
        cache: false,
        dataType: 'json',
        xhrFields: {
            onprogress: function(e) {
                if (e.target.responseText.length < 100) $('.progress-bar').css('width', e.target.responseText.length+'%').html(e.target.responseText.length+'%');
                else $('.progress-bar').css('width', '100%').html('100%');
            }
        },
        complete: function() {

            $('.count-qap').text("Licz mnie te QAP'y");
            $('.progress').hide();
            $('.description').show();
            window.location = 'download.php';
        }
    });

}