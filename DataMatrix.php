<?php
/**
 * Created by PhpStorm.
 * User: kpawla13
 * Date: 2018-05-29
 * Time: 09:16
 */

class DataMatrix
{
    private $instanceSize = 0;
    private $distanceMatrix = [];
    private $flowMatrix = [];

    public function __construct($dataFile)
    {
        $this->fillFromFile($dataFile);
    }

    public function fillFromFile($file)
    {

        try {

            $handle = fopen($file, "r");

            $lineNumber = 0;
            $matrixRow = [];

            if ($handle) {

                while (($line = fgets($handle)) !== false) {

                    if ($lineNumber == 0) {

                        $this->instanceSize = intval(trim($line, ' '));

                        $lineNumber++;
                        continue;
                    } else {
                        $line = preg_replace('/\s+/', ' ', $line);

                        if (strlen(trim($line)) == 0) {
                            $lineNumber++;
                            continue;
                        }

                        $row = explode(' ', trim($line, ' '));
                        $row = array_map('intval', $row);

                        if (count($matrixRow) < $this->instanceSize) {
                            $matrixRow = array_merge($matrixRow, $row);
                        } else {

                            if (count($this->distanceMatrix) < $this->instanceSize) {

                                $this->distanceMatrix[] = $matrixRow;

                            } else {

                                $this->flowMatrix[] = $matrixRow;

                            }

                            $matrixRow = [];
                            if (strlen(trim($line)) > 0 && count($row) > 0) $matrixRow = $row;

                        }

                        $lineNumber++;

                    }
                }

                if (count($matrixRow) > 0) $this->flowMatrix[] = $matrixRow;

                fclose($handle);

            } else {
                throw new Exception('Blad podczas otwierania pliku!');
            }
        } catch (Exception $e) {

            echo $e->getMessage();

            $this->distanceMatrix = [];
            $this->flowMatrix = [];

            exit();

        }

    }

    public function getDistanceMatrix()
    {

        return $this->distanceMatrix;
    }

    public function getFlowMatrix()
    {

        return $this->flowMatrix;
    }

    public function getDistance($row, $col)
    {

        return $this->distanceMatrix[$row][$col];

    }

    public function getFlow($row, $col)
    {

        return $this->flowMatrix[$row][$col];

    }

    public function printDistanceMatrix()
    {

        echo '<h5>Macierz odległości: </h5>';
        echo '<table style="border: 1px solid black; border-collapse: collapse;">';

        foreach ($this->distanceMatrix as $row) {
            echo '<tr style="border: 1px solid black;">';
            foreach ($row as $value) {
                echo '<td style="border: 1px solid black;">' . $value . '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
    }

    public function printFlowMatrix()
    {

        echo '<h5>Macierz przepływu: </h5>';
        echo '<table style="border: 1px solid black; border-collapse: collapse;">';

        foreach ($this->flowMatrix as $row) {
            echo '<tr style="border: 1px solid black;">';
            foreach ($row as $value) {
                echo '<td style="border: 1px solid black;">' . $value . '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
    }

}