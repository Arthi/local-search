<?php
/**
 * Created by PhpStorm.
 * User: kpawla13
 * Date: 2018-05-30
 * Time: 16:41
 */

require_once('./localSearch.php');

class Program
{

    public $solutions = [];

    public static function updateProgress($msg) {

        $step = 100/($_SESSION['rounds']*2);

        for($i = 0; $i < floor($step); $i++) echo $msg;

        flush();
        ob_flush();
    }

    public function startInstance($instanceName, $type, $times, $maxtime = null)
    {
        $this->solutions = [];

        set_time_limit(300);
        error_log('Instance started: '.$instanceName.' ('.$type.')');

        $solutionFile = "./data/".$instanceName.".sln";
        $dataFile = "./data/".$instanceName.".dat";

        $localSearch = new LocalSearch($solutionFile, $dataFile);

        $localSearch->permutationsAnalysed = 0;
        $localSearch->steps = 0;
        $localSearch->originalList = range(0, $localSearch->chainSize - 1);

        if ($type == 'RANDOM') {

            $time_start = microtime(true);
            $instance = 1;

            while (true) {

                $localSearch->permutationsAnalysed++;
                $localSearch->steps++;
                $localSearch->beRandom();

                $thisExecutionTime = (float)microtime(true) - $time_start;
                $localSearch->executionTime += $thisExecutionTime;

                $this->solutions[] = [
                    $localSearch->calculatedOptimumSolution,
                    $thisExecutionTime,
                    implode(',', $localSearch->calculatedOptimumPermutation)
                ];

                if ($localSearch->executionTime >= $maxtime) break;
                $instance++;
            }

            $localSearch->permutationsAnalysed *= $localSearch->permutationsAnalysed;
            $localSearch->steps *= $localSearch->steps;

        } else {

            for ($i = 1; $i <= $times; $i++) {

                $localSearch->calculatedOptimumSolution = null;
                $localSearch->calculatedOptimumPermutation = null;

                $time_start = microtime(true);

                $permutationChain = $localSearch->originalList;
                shuffle($permutationChain);

                $localSearch->generateNeighbours($permutationChain);

                switch ($type) {

                    case 'GREEDY':
                        {

                            $localSearch->beGreedy();
                            break;

                        }
                    case 'STEEPEST':
                        {

                            $localSearch->beSteepest();
                            break;

                        }
                }

                $thisExecutionTime = (float)microtime(true) - $time_start;
                $localSearch->executionTime += $thisExecutionTime;

                $this->solutions[] = [
                    $localSearch->calculatedOptimumSolution,
                    $thisExecutionTime,
                    implode(',', $localSearch->calculatedOptimumPermutation)
                ];

            }

        }

        Program::updateProgress(' ');

        return [$instanceName, $this->solutions, $type, $localSearch->executionTime, $localSearch->optimumSolution, $localSearch->permutationsAnalysed, $localSearch->steps];

    }



    public function putInstanceResultsToCSV($results) {

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="solutions.csv";');

        $f = fopen('php://output', 'w');

        fputcsv($f, [
            'Instancja',
            'Algorytm',
            'Jakosc (%)',
            'Odchylenie (%)',
            'Czas (s)',
            'Srednio krokow',
            'Przejrzane rozwiazania',
            'Probek',
            'Najlepsza probka (%)',
            'Najgorsza probka (%)'
        ], ';');


        foreach ($results as $result) {

            foreach ($result[1] as &$numbers) $numbers[4] = ($result[4]/$numbers[0]);

            $best = $this->getBest($result);
            $worst = $this->getWorst($result);
            $mean = $this->countMean($result);
            $stddev = $this->countDeviation($result, $mean);

            fputcsv($f, [
                $result[0],
                $result[2],
                str_replace('.', ',', ($mean * 100).'%'),
                str_replace('.', ',', $stddev),
                str_replace('.', ',', $result[3]),
                floor($result[6] / count($result[1])),
                floor($result[5] / count($result[1])),
                count($result[1]),
                str_replace('.', ',', ($best * 100).'%'),
                str_replace('.', ',', ($worst * 100).'%')
            ], ';');

        }

        fpassthru($f);

    }

    private function getBest($result) {

        $best = $result[1][0][4];
        foreach($result[1] as $row) {

            if ($best < $row[4]) $best = $row[4];
        }

        return $best;

    }

    private function getWorst($result) {

        $worst = $result[1][0][4];
        foreach($result[1] as $row) {

            if ($worst > $row[4]) $worst = $row[4];
        }

        return $worst;

    }

    private function countMean($result) {

        $sum = 0;
        foreach($result[1] as $row) {
            $sum += $row[4];
        }

        return $sum / count($result[1]);

    }

    private function countDeviation($result, $mean) {

        $sum = 0;

        foreach($result[1] as $row) $sum += pow($row[4] - $mean, 2);

        $dev = sqrt($sum / count($result[1]));

        return $dev;

    }
}