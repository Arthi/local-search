<?php
/**
 * Created by PhpStorm.
 * User: kpawla13
 * Date: 2018-05-28
 * Time: 09:34
 */

session_start();

require_once('./Program.php');
$program = new Program();

$_SESSION['result'] = [];

$result = [];

$_SESSION['rounds'] = 10;

$result[] = $program->startInstance("chr12a", 'GREEDY', $_SESSION['rounds']);
$result[] = $program->startInstance("chr18a", 'GREEDY', $_SESSION['rounds']);
$result[] = $program->startInstance("had12", 'GREEDY', $_SESSION['rounds']);
$result[] = $program->startInstance("had16", 'GREEDY', $_SESSION['rounds']);
$result[] = $program->startInstance("esc16i", 'GREEDY', $_SESSION['rounds']);
$result[] = $program->startInstance("nug14", 'GREEDY', $_SESSION['rounds']);
$result[] = $program->startInstance("nug20", 'GREEDY', $_SESSION['rounds']);
$result[] = $program->startInstance("scr15", 'GREEDY', $_SESSION['rounds']);
$result[] = $program->startInstance("tai17a", 'GREEDY', $_SESSION['rounds']);
$result[] = $program->startInstance("nug12", 'GREEDY', $_SESSION['rounds']);

$result[] = $program->startInstance("chr12a", 'STEEPEST', $_SESSION['rounds']);
$result[] = $program->startInstance("chr18a", 'STEEPEST', $_SESSION['rounds']);
$result[] = $program->startInstance("had12", 'STEEPEST', $_SESSION['rounds']);
$result[] = $program->startInstance("had16", 'STEEPEST', $_SESSION['rounds']);
$result[] = $program->startInstance("esc16i", 'STEEPEST', $_SESSION['rounds']);
$result[] = $program->startInstance("nug14", 'STEEPEST', $_SESSION['rounds']);
$result[] = $program->startInstance("nug20", 'STEEPEST', $_SESSION['rounds']);
$result[] = $program->startInstance("scr15", 'STEEPEST', $_SESSION['rounds']);
$result[] = $program->startInstance("tai17a", 'STEEPEST', $_SESSION['rounds']);
$result[] = $program->startInstance("nug12", 'STEEPEST', $_SESSION['rounds']);

$overtalTimes = [];

for ($i = 0; $i<$_SESSION['rounds']; $i++) {
    $overtalTimes[] = (($result[$i][3] + $result[$i+$_SESSION['rounds']][3]) / 2);
}

$result[] = $program->startInstance("chr12a", 'RANDOM', 0, $overtalTimes[0]);
$result[] = $program->startInstance("chr18a", 'RANDOM', 0, $overtalTimes[1]);
$result[] = $program->startInstance("had12", 'RANDOM', 0, $overtalTimes[2]);
$result[] = $program->startInstance("had16", 'RANDOM', 0, $overtalTimes[3]);
$result[] = $program->startInstance("esc16i", 'RANDOM', 0, $overtalTimes[4]);
$result[] = $program->startInstance("nug14", 'RANDOM', 0, $overtalTimes[5]);
$result[] = $program->startInstance("nug20", 'RANDOM', 0, $overtalTimes[6]);
$result[] = $program->startInstance("scr15", 'RANDOM', 0, $overtalTimes[7]);
$result[] = $program->startInstance("tai17a", 'RANDOM', 0, $overtalTimes[8]);
$result[] = $program->startInstance("nug12", 'RANDOM', 0, $overtalTimes[9]);

$_SESSION['result'] = $result;